import 'package:flutter/material.dart';
import 'package:ortak_harcama/pages/Models/expense.dart';
import 'package:ortak_harcama/pages/landing_page.dart';
import './providers/expense_provider.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => ExpenseProvider(),
      child: MaterialApp(
        title: "Ortak HarcaMa",
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.blueGrey[800],
          accentColor: Colors.grey[200],

        ),
        home: LandingPage(),

      ),
    );
  }
}
