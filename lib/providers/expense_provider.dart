

import 'package:flutter/material.dart';
import '../pages/Models/expense.dart';

class ExpenseProvider with ChangeNotifier {

    List<Expense> _items = [
      new Expense(1, "ev1 yemek123456", 12, "Yemek", false, "sefaköy"),
      new Expense(2, "ev2 yemek", 14, "Etkinlik", true, "sefaköy"),
      new Expense(3, "ev3 yemek", 13, "Icmek", false, "sefaköy"),
      new Expense(4, "ev1 yemek", 12, "Diger", false, "sefaköy"),
      new Expense(5, "ev2 yemek", 14, "Yemek", false, "sefaköy"),
      new Expense(6, "ev3 yemek", 13, "Yemek", false, "sefaköy"),
      new Expense(7, "ev1 yemek", 12, "Yemek", false, "sefaköy"),
      new Expense(8, "ev2 yemek", 14, "Yemek", false, "sefaköy"),
      new Expense(9, "ev3 yemek", 13, "Yemek", false, "sefaköy"),
      new Expense(10, "ev1 yemek", 12, "Yemek", false, "sefaköy"),
      new Expense(11, "ev2 yemek", 14, "Yemek", false, "sefaköy"),
      new Expense(12, "ev3 yemek", 13, "Yemek", false, "sefaköy"),
      new Expense(13, "ev1 yemek", 12, "Yemek", false, "sefaköy"),
      new Expense(14, "ev2 yemek", 14, "Yemek", false, "sefaköy"),
      new Expense(15, "ev3 yemek", 13, "Yemek", false, "sefaköy"),
      new Expense(16, "ev1 yemek", 12, "Yemek", false, "sefaköy"),
      new Expense(17, "ev2 yemek", 14, "Yemek", false, "sefaköy"),
      new Expense(18, "ev3 yemek", 13, "Yemek", false, "sefaköy"),
    ];

   // dart ta herşey referans eger direkt _items donseydik referansı doncekti
    List<Expense> get items {
      debugPrint("Expense Provider GET ITEMS");
      return [..._items];
    }

    Expense findById(String id) {
      return _items.firstWhere((expense) => expense.id.toString() == id);
    }

    void addExpense(Expense expense) {
      _items.add(expense);
      notifyListeners();
    }
    
    
}