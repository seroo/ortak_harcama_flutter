

import 'package:flutter/foundation.dart';

class Participant {

  int _id;
  String _name;
  String _description;
  bool _isJoint;
  bool _isPaid;
  double _amountOfCut;
  DateTime _dateCreated;
  DateTime _dateUpdated;
  int _expenseId;

  Participant(@required String this._name, @required double this._amountOfCut,
      [String this._description, bool this._isJoint, bool this._isPaid]
      ) {
    _dateCreated = DateTime.now();
  }

  double get amountOfCut => _amountOfCut;

  set amountOfCut(double value) {
    _dateUpdated = DateTime.now();
    _amountOfCut = value;
  }

  int get id => _id;

  bool get isPaid => _isPaid;

  set isPaid(bool value) {
    _dateUpdated = DateTime.now();

    _isPaid = value;
  }

  bool get isJoint => _isJoint;

  set isJoint(bool value) {
    _dateUpdated = DateTime.now();

    _isJoint = value;
  }

  String get description => _description;

  set description(String value) {
    _dateUpdated = DateTime.now();

    _description = value;
  }

  String get name => _name;

  set name(String value) {
    _dateUpdated = DateTime.now();

    _name = value;
  }

  @override
  String toString() {
    return 'Participant{_name: $_name, _description: $_description, _isJoint: $_isJoint, _isPaid: $_isPaid, _amountOfCut: $_amountOfCut}';
  }

  DateTime get dateCreated => _dateCreated;

  DateTime get dateUpdated => _dateUpdated;

  int get expenseId => _expenseId;


}