import 'package:flutter/foundation.dart';

class Expense {
  int _id;
  String _place;
  String _description;
  bool _isJoint;
  String _variety;
  double _amount;
  DateTime _dateCreated;
  DateTime _dateUpdated;

  DateTime get dateCreated => _dateCreated;

  Expense(int this._id, @required String this._description, @required double this._amount,
      @required String this._variety,
      [bool this._isJoint, String this._place]) {
    _dateCreated = DateTime.now();
  }

  int get id => _id;

  double get amount => _amount;

  set amount(double value) {
    _dateUpdated = DateTime.now();
    _amount = value;
  }

  String get variety => _variety;

  set variety(String value) {
    _dateUpdated = DateTime.now();
    _variety = value;
  }

  bool get isJoint => _isJoint;

  set isJoint(bool value) {
    _dateUpdated = DateTime.now();
    _isJoint = value;
  }

  String get description => _description;

  set description(String value) {
    _dateUpdated = DateTime.now();
    _description = value;
  }

  String get place => _place;

  set place(String value) {
    _dateUpdated = DateTime.now();
    _place = value;
  }

  @override
  String toString() {
    return 'Expense{_place: $_place, _description: $_description, _isJoint: $_isJoint, _variety: $_variety, _amount: $_amount}';
  }

  DateTime get dateUpdated => _dateUpdated;


}
