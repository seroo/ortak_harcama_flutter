import 'package:flutter/material.dart';
import 'package:ortak_harcama/pages/Models/participant.dart';
import 'package:ortak_harcama/pages/components/expense_card_item.dart';
import 'package:ortak_harcama/pages/expense_detail.dart';
import 'package:provider/provider.dart';
import '../providers/expense_provider.dart';


class HomePage extends StatefulWidget {


  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Participant p = Participant("serhat", 12, "asd");

  @override
  Widget build(BuildContext context) {
    final expenseData = Provider.of<ExpenseProvider>(context);
    final expenses = expenseData.items;
    return Scaffold(
        appBar: AppBar(
          title: Text("Ortak HarcaMa"),
        ),
        body: ListView.builder(
          // Globalkey expensive mis uygulama genelini ilgilendirmiyosa bu yeter
          key: UniqueKey(),
          itemCount: expenses.length,
          itemBuilder: (context, index) {
            return ExpenseCardItem(expenses[index]);
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add, color: Colors.white,),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => ExpenseDetail(-1),
            ));
          },
          backgroundColor: Theme.of(context).primaryColor,
        ),
    );
  }
}
