import 'package:flutter/material.dart';
import 'package:ortak_harcama/pages/Models/expense.dart';
import 'package:ortak_harcama/pages/expense_detail.dart';

class ExpenseCardItem extends StatelessWidget {
  final Expense _expense;

  ExpenseCardItem(this._expense);


  @override
  Widget build(BuildContext context) {
    return Card(

      elevation: 4,
      child: Padding(
        padding: EdgeInsets.all(8),
        child: ListTile(
          leading: CircleAvatar(
            backgroundColor: Theme.of(context).primaryColor,
            child: Text(
              _expense.amount.toStringAsFixed(
                  _expense.amount.truncateToDouble() == _expense.amount
                      ? 0
                      : 2),
              style: TextStyle(color: Colors.white),
            ),
          ),
          title: Text(_expense.description.toString()),
          onTap: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (context) => ExpenseDetail(_expense.id),
            ));
          },
        ),
      ),
    );
  }
}
