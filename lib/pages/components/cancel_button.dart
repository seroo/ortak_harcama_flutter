import 'package:flutter/material.dart';

class CancelButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      //padding: EdgeInsets.fromLTRB(16, 0, 16, 0),

      onPressed: () {},
      textColor: Colors.white,
      padding: const EdgeInsets.all(0.0),
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Colors.pinkAccent,
              Colors.red,
              Colors.redAccent

            ],
          ),
        ),
        padding: const EdgeInsets.all(10.0),
        child: const Text('Cancel'),
      ),
    );
  }
}
