import 'package:flutter/material.dart';
import 'package:ortak_harcama/pages/Models/expense.dart';
import 'package:ortak_harcama/pages/homepage.dart';

class SignIn extends StatefulWidget {


  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  var _formKey = GlobalKey<FormState>();

  final _formEmailController = TextEditingController();
  final _formPassController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _formEmailController.dispose();
    _formPassController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Sign In"),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
              child: TextFormField(
                controller: _formEmailController,
                decoration: InputDecoration(
                  hintText: "email",
                ),
                validator: _checkEmail,
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
              child: TextFormField(
                controller: _formPassController,
                decoration: InputDecoration(
                  hintText: "password",
                ),
                validator: (value) {
                  if (value.length > 3) {
                    return null;
                  } else {
                    return "Password field can't be less than 3 letter";
                  }
                },
              ),
            ),
            Padding(

              padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HomePage(),
                        ),
                      );
                    },
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  RaisedButton(
                      onPressed: () {},
                      textColor: Colors.white,
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.grey[800]),
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  } // build

  String _checkEmail(String mail) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(mail))
      return 'Please type proper email';
    else
      return null;
  }

}
