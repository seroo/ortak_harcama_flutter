import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ortak_harcama/pages/Models/expense.dart';
import 'package:provider/provider.dart';
import '../providers/expense_provider.dart';

class ExpenseDetail extends StatefulWidget {
  int _expenseId;

  ExpenseDetail([this._expenseId]);

  @override
  _ExpenseDetailState createState() => _ExpenseDetailState(_expenseId);
}

class _ExpenseDetailState extends State<ExpenseDetail> {
  int _expenseId;

  final _formDescriptionController = TextEditingController();
  final _formPlaceController = TextEditingController();
  final _formAmountController = TextEditingController();
  bool _isJoint = false;
  static List<String> _varieties = [
    "Yemek",
    "Icmek",
    "Etkinlik",
    "Diger",
  ];
  String _selectedVariety = _varieties[0];
  String _description, _place, _amount;
  var _formKey = GlobalKey<FormState>();

  // Constructor
  _ExpenseDetailState([this._expenseId]);

  @override
  Widget build(BuildContext context) {
    debugPrint("ExpenseId : $_expenseId");

    var expenseData;

    String title;
    if (_expenseId < 0) {
      title = "Add Expense";
    } else {
      // listen argument i degisikligi dinlememesini saglio. bu widget sadece verilen expense i gorecek
      expenseData = Provider.of<ExpenseProvider>(context, listen: false)
          .findById(_expenseId.toString());
      String desc = expenseData.description;
      title = desc.length < 10 ? desc : desc.substring(0, 9) + "...";
      _formDescriptionController.text = expenseData.description;
      _formPlaceController.text = expenseData.place;
      _formAmountController.text = expenseData.amount.toString();
      _isJoint = expenseData.isJoint;
      _selectedVariety = _varieties[_varieties.indexOf(expenseData.variety)];
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      // bunu yapmazsak görüntülenemeyen alan hatası cikiyor
      body: SingleChildScrollView(
        child: Container(
          child: Form(
            key: _formKey,
            child: Column(
//            mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                  child: TextFormField(
                    controller: _formDescriptionController,
                    decoration: InputDecoration(labelText: "Description (*)"),
                    validator: (value) {
                      if (value.length < 2) {
                        return "Description cant be less than 2 char";
                      } else
                        return null;
                    },
                    onSaved: (value) => _description = value,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                  child: TextFormField(
                    controller: _formPlaceController,
                    decoration: InputDecoration(labelText: "Place"),
                    onSaved: (value) => _place = value,
                  ),
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        value: _isJoint,
                        activeColor: Theme.of(context).primaryColor,
                        onChanged: (value) {
                          debugPrint(value.toString());
                          setState(() {
                            _isJoint = value;
                          });
                        },
                      ),
                      Text("Is everybody joint?"),
                    ],
                  ),
                ),
                Container(
                    padding: EdgeInsets.fromLTRB(16, 0, 0, 0),
                    child: DropdownButton(
                      value: _selectedVariety,
                      onChanged: (newVariety) {
                        setState(() {
                          _selectedVariety = newVariety;
                        });
                      },
                      items: _varieties.map((variety) {
                        return new DropdownMenuItem(
                          child: new Text(variety),
                          value: variety,
                        );
                      }).toList(),
                    )),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                  child: TextFormField(
                    controller: _formAmountController,
                    decoration: InputDecoration(labelText: "Amount (*)"),
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    onSaved: (value) => _amount = value,
                    validator: (value) {
                      if (double.parse(value) <= 0) {
                        return "Type the price please";
                      } else
                        return null;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(16, 0, 16, 8),
                  child: RaisedButton(
                    child: Text("Submit"),
                    onPressed: () {
                      _submitForm();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _submitForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      int newIndex = Provider.of<ExpenseProvider>(context, listen: false).items.length + 1;
      Expense newExpense = new Expense(
          newIndex, _description, double.parse(_amount), _selectedVariety, _isJoint, _place
      );
      Provider.of<ExpenseProvider>(context).addExpense(newExpense);
      Navigator.of(context).pop();
    }
  }
}
